<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;

class EntryController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('query');
        $query = Entry::with('tags')->orderBy('english');
        if($search) {
            $query = $query->where('swahili', 'LIKE', '%'.$search.'%')
                ->orWhere('english', 'LIKE', '%'.$search.'%');
        }
        return $query->paginate(15);
    }

    public function destroy(Entry $entry)
    {
        $entry->delete();
        return [
            'message' => 'Entry deleted'
        ];
    }

    public function update(Entry $entry, Request $request)
    {
        $data = $this->validate($request, [
            'swahili' => 'required',
            'english' => 'required',
            'tags.*' => ''
        ]);
        $entry->fill($data);
        $entry->save();
        return [
            'message' => 'Entry updated'
        ];
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'swahili' => 'required',
            'english' => 'required',
            'tags.*' => ''
        ]);
        Entry::create($data);

        return ['message' => 'Entry created'];
    }
}
