<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use \Spatie\Tags\HasTags;

    protected $fillable = ['swahili', 'english', 'tags'];


    public function toArray()
    {
        return [
            'id' => $this->id,
            'swahili' => $this->swahili,
            'english' => $this->english,
            'tags' => $this->tags()->pluck('name')
        ];
    }
}
