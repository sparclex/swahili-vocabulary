<?php

use Faker\Generator as Faker;

$factory->define(\App\Entry::class, function (Faker $faker) {
    return [
        'swahili' => $faker->words(3, true),
        'english' => $faker->words(3, true),
        'tags' => ['tag 1', 'tag 2']
    ];
});
