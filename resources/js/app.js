/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Router from 'vue-router';
import PortalVue from 'portal-vue';
import Toasted from 'vue-toasted';

import VueMultiselect from 'vue-multiselect';

import { directive as clickaway } from 'vue-clickaway';
Vue.directive('onClickaway', clickaway);
Vue.use(Router);
Vue.use(PortalVue);
Vue.use(Toasted, {
    position: 'bottom-right',
    duration: 3000,
    theme: 'sparclex'
});
import Index from './components/Index';
import Modal from './components/Modal';
import Create from './components/Create';
import FilterButton from './components/FilterButton';
import Entry from './components/Entry';
import EditModal from './components/EditModal';
Vue.component('entry', Entry);
Vue.component('edit-modal', EditModal);
Vue.component('filter-button', FilterButton);
Vue.component('modal', Modal);
Vue.component('create', Create);
Vue.component('multiselect', VueMultiselect);

const routes = [
    {
        name: 'index',
        path: '/',
        component: Index,
        props: true,
    },
    {
        name: 'page',
        path: '/page/:page',
        component: Index,
        props: true
    }
];
const router = new Router({
    mode: 'history',
    routes,
})

const app = new Vue({
    el: '#app',
    router
});
