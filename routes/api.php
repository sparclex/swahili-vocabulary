<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/entries', 'EntryController@index');
Route::delete('/entries/{entry}', 'EntryController@destroy');
Route::post('/entries', 'EntryController@store');
Route::put('/entries/{entry}', 'EntryController@update');
Route::get('/tags', function() {
    return \Spatie\Tags\Tag::pluck('name');
});
